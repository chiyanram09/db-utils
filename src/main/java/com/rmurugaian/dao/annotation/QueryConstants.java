package com.rmurugaian.dao.annotation;

/**
 * @author rmurugaian 2018-06-29
 */
public final class QueryConstants {

    public static final String INSERT_QUERY = "INSERT INTO %s ( %s ) VALUES ( %s ) RETURNING ID";
    public static final String FIND_BY_QUERY = "SELECT * FROM %s WHERE %s = :%s";
    public static final String FINA_ALL_QUERY = "SELECT * FROM %s";
    public static final String UPDATE_QUERY = "UPDATE %s SET %s WHERE %s = :%s";
    public static final String DELETE_QUERY = "DELETE FROM %s WHERE %s = :%s";
    public static final String DELETE_ALL_QUERY = "TRUNCATE TABLE %s";

    private QueryConstants() {
    }

}
