package com.rmurugaian.dao.annotation;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.rmurugaian.dao.annotation.QueryConstants.*;
import static com.rmurugaian.dao.annotation.QueryParserFunctions.*;

/**
 * @author rmurugaian 2018-06-29
 */
public abstract class QueryParser<T> {

    private final Class<T> clazz;

    QueryParser(final Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     * @param clazz
     * @param <T>
     * @return
     */
    public String saveQuery() {

        final String name = TABLE_NAME.apply(clazz);

        final Field[] fields = clazz.getDeclaredFields();

        List<String> columns = COLUMN_NAMES.apply(fields);

        if (columns.isEmpty()) {
            columns = Arrays.stream(fields)
                .filter(field -> !"id".equalsIgnoreCase(field.getName()))
                .map(Field::getName)
                .collect(Collectors.toList());
        }

        return String.format(INSERT_QUERY, name, COLUMNS.apply(columns), INSERT_COLUMNS.apply(columns));
    }

    public String findAllQuery() {

        return String.format(FINA_ALL_QUERY, TABLE_NAME.apply(clazz));
    }

    /**
     * @param clazz
     * @param <T>
     * @return
     */
    public String findByIdQuery() {

        final String name = TABLE_NAME.apply(clazz);

        final Field[] fields = clazz.getDeclaredFields();

        final String id = GET_ID_COLUMN_NAME.apply(fields);

        return String.format(FIND_BY_QUERY, name, id, id);
    }

    /**
     * @param clazz
     * @param columnName
     * @param <T>
     * @return
     */
    public String findByQuery(final String columnName) {
        final String name = TABLE_NAME.apply(clazz);

        return String.format(FIND_BY_QUERY, name, columnName, columnName);
    }

    /**
     * @param clazz
     * @param <T>
     * @return
     */
    public String deleteByIdQuery() {

        final String name = TABLE_NAME.apply(clazz);

        final Field[] fields = clazz.getDeclaredFields();

        final String id = GET_ID_COLUMN_NAME.apply(fields);

        return String.format(DELETE_QUERY, name, id, id);
    }

    /**
     * @param clazz
     * @param columnName
     * @param <T>
     * @return
     */
    public String deleteByQuery(final String columnName) {
        final String name = TABLE_NAME.apply(clazz);

        return String.format(DELETE_QUERY, name, columnName, columnName);
    }

    /**
     * @param clazz
     * @param columnName
     * @param <T>
     * @return
     */
    public String deleteAllQuery() {
        final String name = TABLE_NAME.apply(clazz);

        return String.format(DELETE_ALL_QUERY, name);
    }
}
