package com.rmurugaian.dao.annotation;

import io.vavr.API;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.io.Serializable;
import java.util.Map;

/**
 * @author rmurugaian 2018-06-29
 */
public abstract class AbstractCrudJdbcRepository<T, ID extends Serializable> extends QueryParser<T>
    implements CrudRepository<T, ID> {

    private final NamedParameterJdbcOperations jdbcTemplate;
    private final Class<T> clazz;
    private final RowMapper<T> rowMapper;

    AbstractCrudJdbcRepository(
        final NamedParameterJdbcOperations jdbcTemplate,
        final Class<T> clazz,
        final RowMapper<T> rowMapper) {

        super(clazz);

        this.jdbcTemplate = jdbcTemplate;
        this.clazz = clazz;
        this.rowMapper = rowMapper;
    }

    @Override
    public <S extends T> S save(final S entity) {

        final Map<String, Object> results =
            jdbcTemplate
                .queryForMap(saveQuery(), ParameterResolver.allColumnsMap(entity));

        JdbcDaoUtils.updateId(entity, results);

        return entity;
    }

    @Override
    public T findOne(final ID id) {

        return jdbcTemplate.queryForObject(findByIdQuery(), API.Map("ID", id).toJavaMap(), rowMapper);
    }

    @Override
    public boolean exists(final ID id) {
        return false;
    }

    @Override
    public Iterable<T> findAll() {

        return jdbcTemplate.query(findAllQuery(), new MapSqlParameterSource(), rowMapper);
    }

    @Override
    public Iterable<T> findAll(final Iterable<ID> ids) {

        return null;
    }

    @Override
    public long count() {

        return 0;
    }

    @Override
    public void delete(final ID id) {

        jdbcTemplate.queryForObject(deleteByIdQuery(), API.Map("ID", id).toJavaMap(), rowMapper);
    }

    @Override
    public void delete(final T entity) {

        return;
    }

    @Override
    public void deleteAll() {

        final int update = jdbcTemplate.update(deleteAllQuery(), new MapSqlParameterSource());
    }
}
