package com.rmurugaian.dao.annotation;

import java.time.OffsetDateTime;

/**
 * @author rmurugaian 2018-06-29
 */
@Table(name = "STUDENT")
public class Student {

    @Id(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DEPT_NAME")
    private String deptName;

    @Column(name = "CREATED_TIME")
    private OffsetDateTime createdTime;

}
