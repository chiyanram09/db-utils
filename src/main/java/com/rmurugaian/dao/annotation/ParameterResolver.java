package com.rmurugaian.dao.annotation;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * @author rmurugaian 2018-06-29
 */
public final class ParameterResolver {

    /**
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> MapSqlParameterSource allColumnsMap(final T entity) {

        final MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        final Class<?> clazz = entity.getClass();

        final Field[] declaredFields = clazz.getDeclaredFields();

        Arrays.stream(declaredFields)
            .filter(field -> field.isAnnotationPresent(Column.class))
            .forEach(
                field ->
                    mapSqlParameterSource
                        .addValue(
                            QueryParserFunctions.GET_COLUMN_NAME.apply(field),
                            getFieldValue(field, entity)));

        return mapSqlParameterSource;

    }

    public static <T> Object getFieldValue(final Field field, final T entity) {

        try {
            return field.get(entity);
        } catch (final IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

}
