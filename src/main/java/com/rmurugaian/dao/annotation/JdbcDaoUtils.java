package com.rmurugaian.dao.annotation;

import java.lang.reflect.Field;
import java.util.Map;

import static com.rmurugaian.dao.annotation.QueryParserFunctions.GET_ID_COLUMN_NAME;

/**
 * @author rmurugaian 2018-06-29
 */
public final class JdbcDaoUtils {

    /**
     * @param entity
     * @param value
     * @param <T>
     */
    public static <T> void updateId(final T entity, final Map<String, Object> value) {
        try {
            final Class<?> clazz = entity.getClass();
            final Field[] fields = clazz.getDeclaredFields();
            final String id = GET_ID_COLUMN_NAME.apply(fields);
            clazz.getDeclaredField(id).set(entity, value.get(id));
        } catch (final IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

}
