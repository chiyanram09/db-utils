package com.rmurugaian.dao.annotation;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author rmurugaian 2018-06-29
 */
public final class QueryParserFunctions {

    public static final Function<List<String>, String> COLUMNS =
        names -> names.stream().collect(Collectors.joining(", "));

    public static final Function<List<String>, String> INSERT_COLUMNS = columns ->
        columns
            .stream()
            .map(name -> StringUtils.prependIfMissing(name, ":"))
            .collect(Collectors.joining(", "));

    public static final Function<Field[], String> GET_ID_COLUMN_NAME =
        fields ->
            Arrays.stream(fields)
                .filter(field -> field.isAnnotationPresent(Id.class))
                .findFirst()
                .map(field -> field.getDeclaredAnnotation(Id.class).name())
                .orElse("id");

    public static final Function<Field, String> GET_COLUMN_NAME =
        field -> Optional.ofNullable(field)
            .filter(fieldL -> fieldL.isAnnotationPresent(Column.class))
            .map(fieldL -> fieldL.getDeclaredAnnotation(Column.class))
            .map(Column::name)
            .orElse(field.getName());

    public static final Function<Class<?>, String> TABLE_NAME =
        clazz -> Optional
            .ofNullable(clazz)
            .filter(cl -> cl.isAnnotationPresent(Table.class))
            .map(cl -> cl.getDeclaredAnnotation(Table.class))
            .map(Table::name)
            .orElseGet(clazz::getSimpleName);

    public static final Function<Field[], List<String>> COLUMN_NAMES =
        fields -> Arrays.stream(fields)
            .map(GET_COLUMN_NAME)
            .collect(Collectors.toList());

    private QueryParserFunctions() {
    }
}
