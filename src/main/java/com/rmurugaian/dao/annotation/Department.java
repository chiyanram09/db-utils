package com.rmurugaian.dao.annotation;

import java.time.OffsetDateTime;

/**
 * @author rmurugaian 2018-06-29
 */
public class Department {

    private Long id;

    private String name;

    private String location;

    private OffsetDateTime created;

}
