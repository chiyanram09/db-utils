package com.rmurugaian.dao.annotation

import spock.lang.Specification

/**
 * @author rmurugaian 2018-06-29
 */
class QueryParserSpec extends Specification {

    def "SelectQuery with annotations"() {
        when:
        def selectQuery = QueryParser.saveQuery(Student.class)

        then:
        selectQuery == "INSERT INTO STUDENT ( NAME, DEPT_NAME, CREATED_TIME ) VALUES ( :NAME, :DEPT_NAME, :CREATED_TIME ) RETURNING ID"
    }

    def "SelectQuery without annotations"() {
        when:
        def selectQuery = QueryParser.saveQuery(Department.class)

        then:
        selectQuery == "INSERT INTO Department ( name, location, created ) VALUES ( :name, :location, :created ) RETURNING ID"
    }

    def "findById with annotations"() {
        when:
        def selectQuery = QueryParser.findByIdQuery(Student.class)

        then:
        selectQuery == "SELECT * FROM STUDENT WHERE ID = :ID"
    }

    def "findById without annotations"() {
        when:
        def selectQuery = QueryParser.findByIdQuery(Department.class)

        then:
        selectQuery == "SELECT * FROM Department WHERE id = :id"
    }

    def "findBy with annotations"() {
        when:
        def selectQuery = QueryParser.findByQuery(Student.class, "ID")

        then:
        selectQuery == "SELECT * FROM STUDENT WHERE ID = :ID"
    }

    def "findBy without annotations"() {
        when:
        def selectQuery = QueryParser.findByQuery(Department.class, "id")

        then:
        selectQuery == "SELECT * FROM Department WHERE id = :id"
    }
}
